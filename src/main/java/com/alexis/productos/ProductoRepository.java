package com.alexis.productos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


//permiten a la estrategia jpa realizar todas las operaciones crud
@Repository
public interface ProductoRepository extends MongoRepository <ProductoModel, String> {

}
