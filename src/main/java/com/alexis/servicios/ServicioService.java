package com.alexis.servicios;

import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.MongoClient;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ServicioService {
    //driver que se conecta a mongo db
    static MongoCollection<Document> servicios;


    private static MongoCollection<Document> getServiciosCollection(){
        //cadena de conexion
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");

        // necesaria para crear un cliente mongo para interactuar con la bd mongo
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        // crear cliente mongo
        MongoClient mongo = MongoClients.create(settings);
        MongoDatabase database = mongo.getDatabase("dbprod");

        return database.getCollection("servicios");
    }

    public static void  insert(String servicio) throws Exception {
        Document doc = Document.parse(servicio);
        servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }

    public static List getAll(){
        servicios = getServiciosCollection();
        List lst = new ArrayList();
        // si no le pasamos ningun objeto va a devolver un collection de interables de tipo document
        FindIterable<Document> iterDoc = servicios.find();
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lst.add(it.next());
        }
        return lst;

    }

    //documento que contiene los filtros contiene las condiciones y clausulas de las consultas por hacer
    public static List getFiltrados(String filtro) {
        servicios = getServiciosCollection();
        List lst = new ArrayList();
        Document docfiltro = Document.parse(filtro);
        FindIterable<Document> iterDoc = servicios.find(docfiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lst.add(it.next());
        }
        return lst;

    }

    public static void update (String filtro, String servicio){
        servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document doc = Document.parse(servicio);

        servicios.updateOne(docFiltro,doc);
    }

}
